# docker-compose-generate

PHP script para gerar arquivo __docker-compose.yml__ para ambientes docker padrão StudioVisual.


## Instalação

__UBUNTU__


Clone o repositório:

`git clone https://bitbucket.org/gilbertoalbino/docker-compose-generate`


Torne o script executável:

`sudo chmod a+x docker-compose-generate/docker-compose-generate`


Mova para a pasta de binários local:

`sudo cp docker-compose-generate/docker-compose-generate /usr/local/bin/docker-composer-generate`


Remova o repositório clonado:

`rm -rf docker-compose-generate`



## Utilização


Execute o comando `docker-compose-generate` passando um argumento que será o hostname.

`docker-compose-generate HOSTNAME`


__OBS:__ Caso o argumento `HOSTNAME` não seja informado será usado o padrão "_project_".



## Exemplo

Caso seja utilizado um hostname chamado "_studiovisual_" será gerado um arquivo com o conteúdo:


```
studiovisual_db:
  image: mariadb
  container_name: studiovisual_db
  volumes_from:
    - "mariadb-data"
  ports:
    - "3306"
  environment:
    - MYSQL_DATABASE=studiovisual
    - MYSQL_ROOT_PASSWORD=intest

mariadb-data:
  image: tianon/true
  volumes:
    - ./mysql:/var/lib/mysql

default_web:
  image: lramos1994/php-apache-base:latest
  container_name: studiovisual_web
  ports:
    - "80:80"
  links:
    - studiovisual_db
  volumes:
    - ./webroot/:/var/www/html
  environment:
    - MYSQL_HOST=studiovisual_web
    - SITE_ENV=dev_docker
    - APACHE_DOC_ROOT=/var/www/html

```


